Module: Guestbooks

Project Page
============
https://www.drupal.org/project/guestbooks

Description
===========
Guestbooks: This module is developed for Guestbook Entries tracking and display.

Requirements
============
None.

Installation
============
1. Copy the 'guestbooks' module directory in to your Drupal 
'modules' directory and install it as usual.

2. Set the correct permissions under People > Permissions > Guestbooks

3. Simple View with listing guestbook entries has been made available to use.

4. For Version 7.x, Simple block for Guestbook Form has been made 
available to use.

That's it! You are all set to use the module.

Usage
=====
1. Use Form Block as per your choice.

2. Use View for Listing Entries as per your choice.

3. In Version 8.x, for Independent Form of Guestbook to use, we 
suggests 'Form Block' or 'Advanced From Block' modules.

Configuration Settings
======================
1. Just ensure the permissions are correctly set for the module in order
to have it working seemlesly.
